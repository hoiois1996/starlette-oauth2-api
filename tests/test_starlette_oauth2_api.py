import json
import datetime

import pytest
import jose.jwt
from starlette.applications import Starlette
from starlette.testclient import TestClient

from starlette_oauth2_api import AuthenticateMiddleware


@pytest.fixture
def base_app():
    key = "not-secret"
    audience = "audience"
    issuer = "https://example.com/"

    def _get_keys(path):
        return key

    app = Starlette()

    app.add_middleware(
        AuthenticateMiddleware,
        providers={
            "custom": {
                "keys": "https://example.com/tenant-id/v2.0/jwks",
                "issuer": issuer,
                "audience": audience,
            }
        },
        paths={"/"},
        get_keys=_get_keys,
    )
    return app, key, audience, issuer


def app_custom_keys(keys, **kwargs):
    audience = "audience"
    issuer = "https://example.com/"

    def _get_keys(path):
        return keys

    if "get_keys" not in kwargs:
        kwargs["get_keys"] = _get_keys

    app = Starlette()
    app.add_middleware(
        AuthenticateMiddleware,
        providers={
            "custom": {"keys": keys, "issuer": issuer, "audience": audience,}
        },
        paths={"/"},
    )
    return app, audience, issuer


class MiddlewareCheck:
    def __init__(self, app, storage):
        self._app = app
        self._storage = storage

    async def __call__(self, scope, receive, send):
        self._storage["scope"] = scope
        return await self._app(scope, receive, send)


def test_no_header(base_app):
    app, _, _, _ = base_app

    client = TestClient(app)
    response = client.get("/")

    assert response.status_code == 400


def test_wrong_header(base_app):
    app, _, _, _ = base_app

    client = TestClient(app)
    response = client.get("/", headers={"authorization": "Baa "})

    assert response.status_code == 400


def test_all_good(base_app):
    app, key, audience, issuer = base_app

    client = TestClient(app)
    token = jose.jwt.encode(
        {
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(seconds=3600),
            "iat": datetime.datetime.utcnow(),
            "aud": audience,
            "iss": issuer,
        },
        key,
    )
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 404


def test_keys_as_dict(base_app):
    key = "not-secret"
    keys = {"keys": [key]}
    app, audience, issuer = app_custom_keys(keys)

    client = TestClient(app)

    token = jose.jwt.encode(
        {
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(seconds=3600),
            "iat": datetime.datetime.utcnow(),
            "aud": audience,
            "iss": issuer,
        },
        key,
    )
    response = client.get("/", headers={"authorization": f"Bearer {token}"})
    assert response.status_code == 404


def test_check_claims(base_app):
    app, key, audience, issuer = base_app

    storage = {}
    app.add_middleware(MiddlewareCheck, storage=storage)

    client = TestClient(app)
    claims = {
        "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
        "iat": datetime.datetime.utcnow(),
        "aud": audience,
        "iss": issuer,
        "custom": "a custom claim",
    }
    token = jose.jwt.encode(claims, key)
    response = client.get("/", headers={"authorization": f"Bearer {token}"})
    assert response.status_code == 404

    assert storage["scope"]["oauth2-claims"] == claims


def test_has_auth_key(base_app):
    app, key, audience, issuer = base_app

    client = TestClient(app)
    claims = {
        "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
        "iat": datetime.datetime.utcnow(),
        "aud": audience,
        "iss": issuer,
    }
    token = jose.jwt.encode(claims, key, access_token="test_access_token")
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 404


def test_wrong_key(base_app):
    app, _, audience, issuer = base_app

    client = TestClient(app)
    token = jose.jwt.encode(
        {
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(seconds=3600),
            "iat": datetime.datetime.utcnow(),
            "aud": audience,
            "iss": issuer,
        },
        "wrong-key",
    )
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 401
    assert response.json() == {
        "message": {"custom": "Signature verification failed."}
    }


def test_expired(base_app):
    app, key, audience, issuer = base_app

    client = TestClient(app)
    token = jose.jwt.encode(
        {
            "exp": datetime.datetime.utcnow()
            - datetime.timedelta(seconds=1800),
            "iat": datetime.datetime.utcnow()
            - datetime.timedelta(seconds=3600),
            "aud": audience,
            "iss": issuer,
        },
        key,
    )
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 401
    assert response.json() == {"message": "Signature has expired."}


def test_wrong_audience(base_app):
    app, key, _, issuer = base_app

    client = TestClient(app)
    token = jose.jwt.encode(
        {
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(seconds=3600),
            "iat": datetime.datetime.utcnow(),
            "aud": "wrong-audience",
            "iss": issuer,
        },
        key,
    )
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 401
    assert response.json() == {"message": {"custom": "Invalid audience"}}


def test_wrong_issuer(base_app):
    app, key, audience, _ = base_app

    client = TestClient(app)
    token = jose.jwt.encode(
        {
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(seconds=3600),
            "iat": datetime.datetime.utcnow(),
            "aud": audience,
            "iss": "wrong-issuer",
        },
        key,
    )
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 401
    assert response.json() == {"message": {"custom": "Invalid issuer"}}


def test_wrong_signature(base_app):
    app, key, audience, issuer = base_app

    client = TestClient(app)
    token = (
        jose.jwt.encode(
            {
                "exp": datetime.datetime.utcnow()
                + datetime.timedelta(seconds=3600),
                "iat": datetime.datetime.utcnow(),
                "aud": audience,
                "iss": issuer,
            },
            key,
        )
        + "a"
    )  # make the signature wrong
    response = client.get("/", headers={"authorization": f"Bearer {token}"})

    assert response.status_code == 401
    assert response.json() == {
        "message": {"custom": "Signature verification failed."}
    }


def test_public_path(base_app):
    app, _, _, _ = base_app

    client = TestClient(app)
    response = client.get("/public")

    assert response.status_code == 404


def test_default_get_keys():
    app_custom_keys(None, get_keys=None)


def test_wrong_provider():
    with pytest.raises(ValueError) as error:
        AuthenticateMiddleware(
            None,
            providers={
                "custom": {
                    "keys": "https://example.com/tenant-id/v2.0/",
                    "audience": "audience",
                }
            },
        )
    print(type(error))
    print(str(error))
    assert "\"custom\" is missing {\\'issuer\\'}." in str(error)


def test_get_keys():
    keys = AuthenticateMiddleware._download_keys(
        "https://login.microsoftonline.com/common/discovery/v2.0/keys"
    )
    assert "keys" in keys


def test_wrong_configuration():
    with pytest.raises(ValueError):
        # not https
        app_custom_keys("http://example.com")

import urllib.request, json
from typing import List, Optional, Callable, Dict, Set, Tuple, Union

import jose.jwt
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.types import ASGIApp, Receive, Scope, Send


class InvalidToken(Exception):
    """When a token is invalid for all identity providers"""

    def __init__(self, errors):
        super().__init__()
        self.errors = errors


class AuthenticateMiddleware:
    """
    A starlette middleware to authenticate and authorize requests through
    OAuth2 JWT tokens.

    Use paths to add paths that require authentication, e.g.
    `/login/validate`.
    Every route that is path returns 401 if it does not have an authorization
    header with `Bearer {token}` where token is a valid jwt.

    Args:
        providers: must be a dictionary with the following keys:
            * ``uri``: a uri of the openid-configuration
            * ``issuer``: issuer or the tokens
            * ``audience``: audience or the tokens

            If multiple providers are passed, the request is valid if any of the
            providers authenticates the request.

        get_keys: An optional function to download the keys from the
            authentication endpoint
    """

    def __init__(
            self,
            app: ASGIApp,
            providers: Dict[str, Dict[str, str]],
            paths: Optional[Set[str]] = None,
            get_keys: Optional[Callable[[], dict]] = None
    ) -> None:
        self._app: ASGIApp = app
        for provider in providers:
            self._validate_provider(provider, providers[provider])
        self._providers: Dict[str, Dict[str, str]] = providers
        self._get_keys: Callable[[], [dict]] = get_keys or self._download_keys
        self._paths: Set[str] = paths or set()

        # cached attribute
        self._keys: dict = {}

    def _provider_claims(self, provider: str, token: str) -> dict:
        """
        Validates the token and returns its respective claims against a specific
        provider.

        Args:
            provider: The provider with which to decode the token.
            token: The string representing the JWT token.

        Returns:
            A dictionary with the claims of the token.

        Raises:
            jose.ExpiredSignatureError if the token has expired.
            jose.JOSEError if it generally has a problem with the token.
        """
        return jose.jwt.decode(
            token,
            self._provider_keys(provider),
            issuer=self._providers[provider]["issuer"],
            audience=self._providers[provider]["audience"],
            options={"verify_at_hash": False},
        )

    def claims(self, token: str) -> Tuple[str, dict]:
        """
        Validates the token and returns its respective claims. The token can be
        any of the valid providers declared for this middleware.

        Args:
            token: The token string from which to get the claims.

        Returns:
            A tuple containing the provider name that succeeded in decoding the 
            token and the claims inside it.

        Raises:
            InvalidToken: If the token couldn't be decoded by any of the
                providers.
        """
        errors = {}
        for provider in self._providers:
            try:
                return provider, self._provider_claims(provider, token)
            except jose.exceptions.ExpiredSignatureError as error:
                # if the token has expired, it is at least from this provider.
                errors = str(error)
                break
            except jose.exceptions.JOSEError as error:  # the catch-all of Jose
                errors[provider] = str(error)
        raise InvalidToken(errors)

    async def __call__(
        self, scope: Scope, receive: Receive, send: Send
    ) -> None:
        request = Request(scope)

        if request.url.path not in self._paths:
            return await self._app(scope, receive, send)

        # check for authorization header and token on it.
        if ("authorization" in request.headers
                and request.headers["authorization"].startswith("Bearer ")):
            token = request.headers["authorization"][len("Bearer ") :]

            try:
                provider, claims = self.claims(token)
                scope["oauth2-claims"] = claims
                scope["oauth2-provider"] = provider
            except InvalidToken as error:
                response = JSONResponse({"message": error.errors}, status_code=401)
                return await response(scope, receive, send)

        elif "authorization" in request.headers:
            response = JSONResponse(
                {
                    "message": 'The "authorization" header must start with "Bearer "'
                },
                status_code=400,
            )
            return await response(scope, receive, send)

        else:
            response = JSONResponse(
                {
                    "message": 'The request does not contain an "authorization" header'
                },
                status_code=400,
            )
            return await response(scope, receive, send)

        return await self._app(scope, receive, send)

    def _provider_keys(self, provider: str) -> dict:
        """
        Returns the jwt keys of the provider. This is retrieved from the internet
        and cached at the first retrieval.

        Args:
            provider: The string of the provider.

        Returns:
            The keys of the given provider.
        """
        if self._keys.get(provider, None) is None:
            self._keys[provider] = self._get_keys(
                self._providers[provider]["keys"]
            )
        return self._keys[provider]

    @staticmethod
    def _validate_provider(
            provider_name: str,
            provider: Dict[str, Dict[str, str]]
    ) -> None:
        """
        Checks that all the keys are in the provider dictionary.

        Args:
            provider_name: The key of the provider, e.g. 'google'.
            provider: The dictionary of providers.
        """
        mandatory_keys = {"issuer", "keys", "audience"}
        if not mandatory_keys.issubset(set(provider)):
            raise ValueError(
                ('Each provider must contain the following keys: '
                 f'{mandatory_keys}. Provider "{provider_name}" is missing '
                 f'{mandatory_keys - set(provider)}.')
            )

        keys = provider["keys"]
        if isinstance(keys, str) and keys.startswith("http://"):
            raise ValueError(
                ('When "keys" is a url, it must start with "https://". This is '
                 f'not true in the provider "{provider_name}"')
            )

    @staticmethod
    def _download_keys(url_or_keys: Union[dict, str]) -> dict:
        """
        Downloads the keys from the server.

        Args:
            url_or_keys: Either the dictionary of keys, or the URL to obtain
                them.

        Returns:
            The dictionary of keys.
        """
        if isinstance(url_or_keys, str) and url_or_keys.startswith("https://"):
            with urllib.request.urlopen(url_or_keys) as response:
                return json.loads(response.read().decode())
        else:
            return url_or_keys
